
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private Player win;

    public Table(Player x, Player o) {
        this.x = x;
        this.o = o;
        Random random = new Random();
        if(random.nextInt(2)+1==1){
            currentPlayer=x;
        }
        else{
            currentPlayer=o;
        }
    }
    
    
    public char[][] getTable() {
        return table;
    }

    public void switchTurn() {
        if (currentPlayer == x) {
            currentPlayer = o;
        } else {
            currentPlayer = x;
        }
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean checkRow() {
        for (int row = 0; row < table.length; row++) {
            if (table[row][0] == table[row][1]
                    && table[row][0] == table[row][2]
                    && table[row][0] != '-') {
                return true;
            }
        }
        return false;
    }
    
    public boolean checkCol(){
        for (int col = 0; col < table.length; col++) {
            if (table[0][col] == table[1][col]
                    && table[0][col] == table[2][col]
                    && table[0][col] != '-') {
                return true;
            }
        }
        return false;
    }
    
    public boolean checkCross1(){
        for(int cross1 = 0;cross1<table.length;cross1++){
            if(table[0][0] == table[1][1] 
                    && table[1][1] == table[2][2] 
                    && table[0][0] != '-'){
                return true;
            }
        }
        return false;
    }
    
    public boolean checkCross2(){
        for(int cross2 = 0;cross2 < table.length; cross2++){
            if(table[0][2] == table[1][1] 
                    && table[1][1] == table[2][0] 
                    && table[0][2] != '-'){
                return true;
            }
        }
        return false;
    }

    public void setRowCol(int row,int col)throws Exception{
        if(this.getTable()[row-1][col-1]=='-'){
           table[row-1][col-1] = currentPlayer.getName();
        }
        else{
            throw new Exception();
        }
    }
    
    public boolean checkWin(){
        if(checkRow() || checkCol() || checkCross1() || checkCross2()){
            win = currentPlayer;
            if(currentPlayer == x){
                x.win();
                o.lose();
            }
            else{
                o.win();
                x.lose();
            }
            return true;
        }
        return false;
    }
    
    public boolean checkDraw(){
        int count = 0;
        for(int i=0; i<table.length;i++){
            for(int j=0;j<table.length;j++){
                 if(table[i][j] != '-'){
                     count++;
                 }
            }
        }
        if(count == 9 && !checkWin()){
            x.graw();
            o.graw();
            return true;
        }
        return false;
    
    }
        


}
