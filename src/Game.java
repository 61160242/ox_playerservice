

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
import java.util.*;

public class Game {

    private Scanner kb = new Scanner(System.in);
    private int row;
    private int col;
    private Table tableChar;
    private Player o;
    private Player x;

    public Game() {
        x = new Player('X');
        o = new Player('O');
    }

    public void showWin() {
        System.out.println("Player " + tableChar.getCurrentPlayer().getName() + " win..");
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTurn() {
        System.out.println("Turn " + tableChar.getCurrentPlayer().getName());
    }

    public void showBye() {
        System.out.println("Bye Bye...");
    }

    public void showTable() {
        for (int i = 0; i < tableChar.getTable().length; i++) {
            for (int j = 0; j < tableChar.getTable()[i].length; j++) {
                System.out.print(tableChar.getTable()[i][j] + " ");
            }
            System.out.println();
        }
    }
    public void showScore(Player player) {
        System.out.println("Player "+player.getName()+" : win = " + 
                player.getWin() + " , lose = " + player.getLose()
                + " , draw = " + player.getDraw());
    }




    public void inputRowCol() {
        try {

            System.out.print("Please input Row Col : ");
            row = kb.nextInt();
            col = kb.nextInt();
            tableChar.setRowCol(row, col);

        } catch (Exception e) {
            showWrong();
            kb.nextLine();
            showTurn();
            inputRowCol();
        }
    }

    public void startGame() {
        tableChar = new Table(x, o);
    }

    public void run() {
        this.startGame();
        this.showWelcome();
        do {
            this.showTable();
            this.showTurn();
            this.runAgain();
            this.startGame();
        } while (inputContinue());
        this.showBye();
    }

    public boolean inputContinue() {
        System.out.println("Plase input continue (y/n) : ");
        char input = kb.next().charAt(0);
        while (input != 'y' && input != 'n') {
            input = kb.next().charAt(0);
        }
        if (input == 'y') {
            return true;
        }
        return false;
    }

    public static void showWrong() {
        System.out.println("Please input position again");
    }

    public void showDraw() {
        System.out.println("Draw!!");
    }
    

    public void runAgain() {
        while (true) {
            this.inputRowCol();
            this.showTable();
            if (tableChar.checkWin()) {
                this.showWin();
                break;
            } else if (tableChar.checkDraw()) {
                this.showDraw();
                break;
            }
            tableChar.switchTurn();
            this.showTurn();
        }
    }

    
}
